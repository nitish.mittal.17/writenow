module.exports = {
    redisDataKeys: {
        PAPER_PURPOSES: "paper_purposes",
        VENDOR_PRICE_PER_PAGE_PREFIX: "vendorPricePerPage::",
        ADMIN_PRICE_PER_PAGE: "adminPricePerPage"
    },
    paperStatus: {
        PENDING: 0,
        COMPLETED: 1,
        BILLED: 2
    },
    adminUserId: 1,
    roles: {
        USER: 1,
        WRITER: 2,
        VENDOR: 3,
        ADMIN: 4
    },
    menuItems: {
        ADMIN: {
            OVERVIEW: {
                id: "overview",
                label: "Overview",
                href: "/app/admin/overview"
            }
        },
        VENDOR: {
            OVERVIEW: {
                id: "overview",
                label: "Overview",
                href: "/app/vendor/overview"
            },
            MANAGE_USERS: {
                id: "manageusers",
                label: "Manage Users",
                href: "/app/vendor/manageusers"
            },
            BILLING: {
                id: "billing",
                label: "Billing",
                href: "/app/vendor/billing"
            }
        },
        WRITER: {
            OVERVIEW: {
                id: "overview",
                label: "Overview",
                href: "/app/writer/overview"
            },
            MANAGE_PAPERS: {
                id: "managepapers",
                label: "Manage Papers",
                href: "/app/writer/managepapers"
            }
        },
        USER: {
            OVERVIEW: {
                id: "overview",
                label: "Overview",
                href: "/app/user/overview"
            },
            MANAGE_PAPERS: {
                id: "managepapers",
                label: "Manage Papers",
                href: "/app/user/managepapers"
            },
            BILLING: {
                id: "billing",
                label: "Billing",
                href: "/app/user/billing"
            }
        }
    }
};
