var GlobalUtils = {
	showAlert: function(message, type) {
        var $alertDiv = $('<div id="global-alert"><div class="alert alert-' + type +'">' + message + '</div></div>');
        $("body").append($alertDiv);
        setTimeout(function() {
            $alertDiv.fadeOut("slow", function() {
                $alertDiv.remove();
            });
        }, 3000);
    }
};

var PaperEditor = function(options) {
    this.options = options;
    this.paperData = "";
    this.messages = "";

    this.init();

    if(this.options.edit == true) {
        this.options.saveBtn.bind("click", $.proxy(this.savePaper, this));
    }

    this.getMessages();
    this.options.sendMessageBtn.bind("click", $.proxy(this.writeMessage, this));

    var obj = this;
    this.options.writeMessageContainer.keypress(function(e) {
        // trap the return key being pressed
        if (e.keyCode === 13) {
            obj.writeMessage();
            return false;
        }
    });

};

PaperEditor.prototype.init = function() {
    this.renderPaper();
};

PaperEditor.prototype.renderPaper = function() {
    var obj = this;
    $.ajax({
        url: "/api/paper/get",
        data: {
            id: this.options.paperId
        },
        method: 'GET',
        success: function(res) {
            obj.options.paperDataContainer.html(res.content);
        }
    })
};

PaperEditor.prototype.getMessages = function() {
    var obj = this;
    $.ajax({
        url: "/api/paper/getMessages",
        method: "GET",
        data: {
            paperId: this.options.paperId
        },
        success: function(res) {
            obj.messages = res;
            obj.renderMessages();
        },
        error: function() {
            GlobalUtils.showAlert("There was some error in getting messages. Please try again", "danger");
        }
    })
};

PaperEditor.prototype.renderMessages = function() {
    this.options.messagesContainer.html('');
    for(var i in this.messages) {
        var html = "<div class='message " + (this.messages[i].userType == 'self' ? "pull-right" : "pull-left") + "'>" + this.messages[i].message + "</div>";
        html += "<div class='clearfix'></div>";
        this.options.messagesContainer.append(html);
    }
    this.options.messagesContainer.scrollTop(this.options.messagesContainer[0].scrollHeight);
};

PaperEditor.prototype.writeMessage = function() {
    var message = this.options.writeMessageContainer.html();
    var obj = this;
    if(message != "") {
        $.ajax({
            url: "/api/paper/createMessage",
            data: {
                message: message,
                paperId: this.options.paperId
            },
            method: 'POST',
            success: function() {
                obj.messages.push({
                    message: message,
                    userType: "self"
                });
                obj.renderMessages();
                obj.options.writeMessageContainer.html('');
            },
            error: function() {
                GlobalUtils.showAlert("Something wrong happened. Please try again", "danger");
            }
        })
    }
};

PaperEditor.prototype.savePaper = function() {
    $.ajax({
        url: "/api/paper/save",
        data: {
            paperId: this.options.paperId,
            content: this.options.paperDataContainer.html()
        },
        method: 'POST',
        success: function() {
            GlobalUtils.showAlert("Data saved successfully", "success");
        },
        error: function() {
            GlobalUtils.showAlert("Problem saving data. Please try again", "danger");
        }
    })
};