var redisUtils = require('./../services/redisUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var util = require('util');
var FileConversionUtils = require('./../services/fileConversionUtils');

var managePaperUrl = Constants.menuItems.USER.MANAGE_PAPERS.href;

module.exports = {
    createPaper: function* (next) {
        var title = this.request.body.title;
        var purposeId = this.request.body.purpose;
        var userId = this.currentUser.id;
        var vendorId = this.currentUser.vendor_id;
        var numOfDays = this.request.body.numOfDays;

        if(this.currentUser.role_id == Constants.roles.USER) {
            var queryString = "insert into paper(title, num_of_days, purpose_id, user_id, vendor_id) values('%s', %s, %s, %s, %s)";
            var query = util.format(queryString, title, numOfDays, purposeId, userId, vendorId);
            yield databaseUtils.executeQuery(query);

            this.redirect(managePaperUrl);
        }
    },

    getPaperData: function* (next) {
        var paperId = this.request.query.id;

        var queryString = "select id, title, content from paper where id = " + paperId;
        var result = yield databaseUtils.executeQuery(queryString);
        var paperDetails = result[0];

        this.type = "application/json";
        this.body = paperDetails;
    },

    savePaperData: function* (next) {
        var paperId = this.request.body.paperId;
        var content = this.request.body.content;

        var queryString = "update paper set content = '%s' where id = %s";
        var query = util.format(queryString, content, paperId);
        yield databaseUtils.executeQuery(query);

        this.body = "OK";
    },

    createMessage: function* (next) {
        var paperId = this.request.body.paperId;
        var userId = this.currentUser.id;
        var message = this.request.body.message;

        var queryString = "insert into user_message(message, user_id, paper_id) values('%s', %s, %s)";
        var query = util.format(queryString, message, userId, paperId);
        yield databaseUtils.executeQuery(query);

        this.body = "OK";
    },

    getMessages: function* (next) {
        var paperId = this.request.query.paperId;

        var queryString = "select * from user_message where paper_id = %s order by creation_timestamp";
        var query = util.format(queryString, paperId);
        var messageList = yield databaseUtils.executeQuery(query);

        for(var i in messageList) {
            if(messageList[i].user_id == this.currentUser.id) {
                messageList[i].userType = "self";
            } else {
                messageList[i].userType = "other";
            }
            delete messageList[i].user_id;
        }

        this.type = "application/json";
        this.body = messageList;
    },

    markPaperAsComplete: function* (next) {
        var paperId = this.request.body.paperId;

        var queryString = "update paper set status = %s where id = %s";
        var query = util.format(queryString, Constants.paperStatus.COMPLETED, paperId);
        yield databaseUtils.executeQuery(query);

        //Create pdf & word documents
        queryString = "select * from paper where id = " + paperId;
        var result = yield databaseUtils.executeQuery(queryString);
        var paperDetails = result[0];

        var filename = paperDetails.title.replace(/ /g, "-");

        var pageCount = yield FileConversionUtils.convertHtmlToPdf(paperDetails.content, filename);
        yield FileConversionUtils.convertHtmlToWord(paperDetails.content, filename);

        queryString = "update paper set num_of_pages = %s, export_filename = '%s' where id = %s";
        query = util.format(queryString, pageCount, filename, paperId);
        yield databaseUtils.executeQuery(query);

        this.body = "OK";
    },

    payBill: function* (next) {
        var paperId = this.request.body.paperId;

        var queryString = "select * from paper where id = %s";
        var query = util.format(queryString, paperId);
        var result = yield databaseUtils.executeQuery(query);
        var pageDetails = result[0];

        var vendorPricePerPage = yield redisUtils.getItem(Constants.redisDataKeys.VENDOR_PRICE_PER_PAGE_PREFIX + this.currentUser.vendor_id);
        var adminPricePerPage = yield redisUtils.getItem(Constants.redisDataKeys.ADMIN_PRICE_PER_PAGE);

        var amountToBeCreditedInAdminAccount = pageDetails.num_of_pages * adminPricePerPage;
        var amountToBeCreditedInVendorAccount = (pageDetails.num_of_pages * vendorPricePerPage) - amountToBeCreditedInAdminAccount;

        queryString = "insert into transaction(amount, beneficiary_account_user_id, payee_user_id) values(%s, %s, %s)";
        query = util.format(queryString, amountToBeCreditedInAdminAccount, Constants.adminUserId, this.currentUser.vendor_id);
        yield databaseUtils.executeQuery(query);

        queryString = "insert into transaction(amount, beneficiary_account_user_id, payee_user_id) values(%s, %s, %s)";
        query = util.format(queryString, amountToBeCreditedInVendorAccount, this.currentUser.vendor_id, this.currentUser.id);
        yield databaseUtils.executeQuery(query);

        queryString = "update paper set status = %s where id = %s";
        query = util.format(queryString, Constants.paperStatus.BILLED, paperId);
        yield databaseUtils.executeQuery(query);

        this.body = "OK";
    }
}

