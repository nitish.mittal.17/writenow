var sessionUtils = require('./../../services/sessionUtils');
var Constants = require('./../../constants');
var config = require('./../../config');
var databaseUtils = require('./../../services/databaseUtils');
var util = require('util');

module.exports = {
    assignWriterToPaper: function* (next) {
        var paperId = this.request.body.paperId;
        var writerId = this.request.body.writerId;

        var queryString = "update paper set writer_id = %s where id = %s and vendor_id = %s";
        var query = util.format(queryString, writerId, paperId, this.currentUser.id);
        yield databaseUtils.executeQuery(query);

        this.body = 100;
    }

}
