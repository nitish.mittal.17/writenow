var sessionUtils = require('./../../services/sessionUtils');
var Constants = require('./../../constants');
var config = require('./../../config');
var databaseUtils = require('./../../services/databaseUtils');
var util = require('util');

module.exports = {

    showOverviewPage: function* (next) {

        var vendorId = this.currentUser.id;

        var queryString = "select p.*, u.name as username from paper p join user u on p.user_id = u.id where p.vendor_id = %s and p.writer_id is null and p.deleted = false";
        var query = util.format(queryString, vendorId);
        var paperList = yield databaseUtils.executeQuery(query);

        queryString = "select u.* from user u where vendor_id = %s and role_id = %s";
        query = util.format(queryString, vendorId, Constants.roles.WRITER);
        var writerList = yield databaseUtils.executeQuery(query);

        yield this.render('vendor/overview', {
            menuItems: Constants.menuItems.VENDOR,
            activeTabId: Constants.menuItems.VENDOR.OVERVIEW.id,
            paperList: paperList,
            writerList: writerList
        })
    },

    showManageUsersPage: function* (next) {

        var vendorId = this.currentUser.id;

        var queryString = "select u.*, r.name as roleName from user u join role r on u.role_id = r.id where u.vendor_id = %s and u.active = true and u.deleted = false";
        var query = util.format(queryString, vendorId);
        var userList = yield databaseUtils.executeQuery(query);

        yield this.render('vendor/manageusers', {
            menuItems: Constants.menuItems.VENDOR,
            activeTabId: Constants.menuItems.VENDOR.MANAGE_USERS.id,
            userList: userList
        })
    },

    showBillingPage: function* (next) {
        yield this.render('vendor/billing', {
            menuItems: Constants.menuItems.VENDOR,
            activeTabId: Constants.menuItems.VENDOR.BILLING.id
        })
    }
}
