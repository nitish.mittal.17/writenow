var sessionUtils = require('./../../services/sessionUtils');
var Constants = require('./../../constants');
var config = require('./../../config');
var databaseUtils = require('./../../services/databaseUtils');
var util = require('util');

var manageUserUrl = Constants.menuItems.VENDOR.MANAGE_USERS.href;

module.exports = {
    createUser: function* (next) {
        var name = this.request.body.name;
        var email = this.request.body.email;
        var roleId = this.request.body.roleId;
        var password = this.request.body.password;
        var vendorId = this.currentUser.id;

        var queryString = "insert into user(name, email, password, role_id, vendor_id) values('%s', '%s', '%s', %s, %s)";
        var query = util.format(queryString, name, email, password, roleId, vendorId);
        yield databaseUtils.executeQuery(query);

        this.redirect(manageUserUrl);
    }
}
