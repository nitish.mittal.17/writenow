var redisUtils = require('./../../services/redisUtils');
var Constants = require('./../../constants');
var config = require('./../../config');
var databaseUtils = require('./../../services/databaseUtils');
var util = require('util');

module.exports = {

    showOverviewPage: function* (next) {
        yield this.render('writer/overview', {
            menuItems: Constants.menuItems.WRITER,
            activeTabId: Constants.menuItems.WRITER.OVERVIEW.id
        })
    },

    showManagePapersPage: function* (next) {

        var writerId = this.currentUser.id;

        var queryString = "select * from paper where writer_id = %s and status = %s";
        var query = util.format(queryString, writerId, Constants.paperStatus.PENDING);
        var paperList = yield databaseUtils.executeQuery(query);

        var paperPurposes = yield redisUtils.getItem(Constants.redisDataKeys.PAPER_PURPOSES);

        yield this.render('writer/managepapers', {
            menuItems: Constants.menuItems.WRITER,
            activeTabId: Constants.menuItems.WRITER.MANAGE_PAPERS.id,
            paperList: paperList,
            paperPurposes: paperPurposes
        })
    },

    showEditPaperPage: function* (next) {
        var paperId = this.params.paperId;

        yield this.render('writer/editPaper', {
            menuItems: Constants.menuItems.WRITER,
            activeTabId: Constants.menuItems.WRITER.MANAGE_PAPERS.id,
            paperId: paperId
        })
    }
}
