var sessionUtils = require('./../services/sessionUtils');
var Constants = require('./../constants');
var config = require('./../config');
var databaseUtils = require('./../services/databaseUtils');
var util = require('util');

module.exports = {
    showLoginPage: function* (next) {
        var errorMessage;
        yield this.render('login', {
            errorMessage: errorMessage
        });
    },

    login: function* (next) {
        var email = this.request.body.email;
        var password = this.request.body.password;

        var queryString = "select * from user where email = '%s' and password = '%s'";
        var query = util.format(queryString, email, password);
        var results = yield databaseUtils.executeQuery(query);

        var errorMessage;
        if(results.length == 0) {
            errorMessage = "Incorrect user credentials";
            yield this.render('login', {
                errorMessage: errorMessage
            });
        } else {
            var redirectUrl = "";
            var roleId = results[0].role_id;
            if(roleId == Constants.roles.USER) {
                redirectUrl = Constants.menuItems.USER.MANAGE_PAPERS.href;
            } else if(roleId == Constants.roles.VENDOR) {
                redirectUrl = Constants.menuItems.VENDOR.OVERVIEW.href;
            } else if(roleId == Constants.roles.WRITER) {
                redirectUrl = Constants.menuItems.WRITER.MANAGE_PAPERS.href;
            } else if(roleId == Constants.roles.ADMIN) {
                redirectUrl = Constants.menuItems.ADMIN.OVERVIEW.href;
            }
            sessionUtils.saveUserInSession(results[0], this.cookies);

            this.redirect(redirectUrl);
        }
    },

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        this.redirect('/');
    }
}
