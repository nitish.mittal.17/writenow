var redisUtils = require('./../../services/redisUtils');
var Constants = require('./../../constants');
var config = require('./../../config');
var databaseUtils = require('./../../services/databaseUtils');
var util = require('util');

module.exports = {

    showOverviewPage: function* (next) {
        yield this.render('user/overview', {
            menuItems: Constants.menuItems.USER,
            activeTabId: Constants.menuItems.USER.OVERVIEW.id
        })
    },

    showManagePapersPage: function* (next) {

        var userId = this.currentUser.id;

        var queryString = "select p.*, u.name as writerName from paper p left join user u on p.writer_id = u.id where p.user_id = %s and p.deleted = false";
        var query = util.format(queryString, userId);
        var paperList = yield databaseUtils.executeQuery(query);

        var paperPurposes = yield redisUtils.getItem(Constants.redisDataKeys.PAPER_PURPOSES);

        yield this.render('user/managepapers', {
            menuItems: Constants.menuItems.USER,
            activeTabId: Constants.menuItems.USER.MANAGE_PAPERS.id,
            paperList: paperList,
            paperPurposes: paperPurposes
        })
    },

    showReviewPaperPage: function* (next) {
        var paperId = this.params.paperId;

        yield this.render('user/reviewPaper', {
            menuItems: Constants.menuItems.USER,
            activeTabId: Constants.menuItems.USER.MANAGE_PAPERS.id,
            paperId: paperId
        })
    },

    showBillingPage: function* (next) {

        var userId = this.currentUser.id;

        var queryString = "select * from paper where user_id = %s and status = %s";
        var query = util.format(queryString, userId, Constants.paperStatus.COMPLETED);
        var paperList = yield databaseUtils.executeQuery(query);

        var pricePerPage = yield redisUtils.getItem(Constants.redisDataKeys.VENDOR_PRICE_PER_PAGE_PREFIX + this.currentUser.vendor_id);

        var paperPurposes = yield redisUtils.getItem(Constants.redisDataKeys.PAPER_PURPOSES);

        yield this.render('user/billing', {
            menuItems: Constants.menuItems.USER,
            activeTabId: Constants.menuItems.USER.BILLING.id,
            paperList: paperList,
            pricePerPage: pricePerPage,
            paperPurposes: paperPurposes
        })
    }
}
