var databaseUtils = require('./services/databaseUtils');
var redisUtils = require('./services/redisUtils');
var logger = require('./logger');
var Constants = require('./constants');
var fs = require('fs');

function storePaperPurposesInRedis() {
    var query = "select id, purpose from paper_purpose";
    databaseUtils.executePlainQuery(query, function(err, response) {
        if(err) {
            logger.logError(err);
        } else {

            var result = {};
            for(var i in response) {
                result[response[i].id] = response[i].purpose;
            }

            redisUtils.setItem(Constants.redisDataKeys.PAPER_PURPOSES, JSON.stringify(result));
        }
    })
}

function storeAdminPricePerPageInRedis() {
    var query = "select price_charged_per_page from user where id = 1";
    databaseUtils.executePlainQuery(query, function(err, response) {
        if(err) {
            logger.logError(err);
        } else {
            redisUtils.setItem(Constants.redisDataKeys.ADMIN_PRICE_PER_PAGE, response[0].price_charged_per_page);
        }
    });
}

function storeVendorPricePerPageInRedis() {
    var query = "select id, price_charged_per_page from user where role_id = " + Constants.roles.VENDOR;
    databaseUtils.executePlainQuery(query, function(err, response) {
        if(err) {
            logger.logError(err);
        } else {
            for(var i in response) {
                redisUtils.setItem(Constants.redisDataKeys.VENDOR_PRICE_PER_PAGE_PREFIX + response[i].id, response[i].price_charged_per_page);
            }
        }
    });
}

storePaperPurposesInRedis();
storeAdminPricePerPageInRedis();
storeVendorPricePerPageInRedis();