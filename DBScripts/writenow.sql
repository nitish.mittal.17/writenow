-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: writenow
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `paper`
--

DROP TABLE IF EXISTS `paper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) DEFAULT NULL,
  `num_of_days` int(10) unsigned DEFAULT NULL,
  `purpose_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `writer_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `content` mediumtext,
  `deleted` tinyint(4) DEFAULT '0',
  `status` int(10) unsigned DEFAULT '0',
  `num_of_pages` int(11) DEFAULT '0',
  `export_filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_paper_user_idx` (`user_id`),
  KEY `FK_paper_writer_idx` (`writer_id`),
  KEY `FK_paper_purpose_idx` (`purpose_id`),
  KEY `FK_paper_vendor_idx` (`vendor_id`),
  CONSTRAINT `FK_paper_purpose` FOREIGN KEY (`purpose_id`) REFERENCES `paper_purpose` (`id`),
  CONSTRAINT `FK_paper_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_paper_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_paper_writer` FOREIGN KEY (`writer_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper`
--

LOCK TABLES `paper` WRITE;
/*!40000 ALTER TABLE `paper` DISABLE KEYS */;
INSERT INTO `paper` VALUES (1,'2017-05-11 13:10:12','Test Paper 1 (Writer Assigned - Writing in progress)',30,3,4,3,2,'<div><b>What is Lorem Ipsum?</b></div><div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div><div><br></div><div><b>Where does it come from?</b></div><div>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32.</div><div><br></div><div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>',0,0,0,NULL),(2,'2017-05-11 13:10:25','Test Paper 2 (Writer requested)',25,4,4,NULL,2,NULL,0,0,0,NULL),(3,'2017-05-14 05:47:17','Test Paper 3 (Writing Complete - Bill Pending)',20,3,4,3,2,'<div><b>What is Lorem Ipsum?</b></div><div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div><div><br></div><div><br></div><div><b>Where does it come from?</b></div><div>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32.</div><div><br></div><div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>',0,1,1,'Test-Paper-3-(Writing-Complete---Bill-Pending)'),(4,'2017-05-14 05:48:52','Test Paper 4 (Bill paid - Ready for download)',30,5,4,3,2,'<div><b>What is Lorem Ipsum?</b></div><div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div><div><br></div><div><b><br></b></div><div><b>Where does it come from?</b></div><div>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32.</div><div><br></div><div>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>',0,2,1,'Test-Paper-4-(Bill-paid---Ready-for-download)');
/*!40000 ALTER TABLE `paper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper_purpose`
--

DROP TABLE IF EXISTS `paper_purpose`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper_purpose` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `purpose` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper_purpose`
--

LOCK TABLES `paper_purpose` WRITE;
/*!40000 ALTER TABLE `paper_purpose` DISABLE KEYS */;
INSERT INTO `paper_purpose` VALUES (1,'2017-05-10 18:07:44','Graduate Courses'),(2,'2017-05-10 18:07:44','Research Projects'),(3,'2017-05-10 18:07:44','PhD Thesis'),(4,'2017-05-10 18:07:44','Embassy Interviews'),(5,'2017-05-10 18:07:44','Professional Services'),(6,'2017-05-10 18:07:44','Others');
/*!40000 ALTER TABLE `paper_purpose` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'2017-05-09 17:06:57','User'),(2,'2017-05-09 17:06:57','Writer'),(3,'2017-05-09 17:06:57','Vendor'),(4,'2017-05-09 17:06:57','Admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `beneficiary_account_user_id` int(10) unsigned DEFAULT NULL,
  `payee_user_id` int(10) unsigned DEFAULT NULL,
  `amount` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_transaction_user_idx` (`beneficiary_account_user_id`),
  KEY `FK_transaction_payee_user_idx` (`payee_user_id`),
  CONSTRAINT `FK_transaction_payee_user` FOREIGN KEY (`payee_user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_transaction_user` FOREIGN KEY (`beneficiary_account_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (1,'2017-05-14 06:21:13',1,2,10),(2,'2017-05-14 06:21:13',2,4,20);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `vendor_id` int(10) unsigned DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `deleted` tinyint(4) DEFAULT '0',
  `price_charged_per_page` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_user_role_idx` (`role_id`),
  KEY `fk_user_vendor_idx` (`vendor_id`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_user_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2017-05-09 17:11:23','admin@writenow.com','password','Admin',4,NULL,1,0,10),(2,'2017-05-09 17:11:23','vendor@round.glass','password','Round Glass Vendor',3,NULL,1,0,30),(3,'2017-05-09 17:11:23','writer@round.glass','password','Writer 1',2,2,1,0,0),(4,'2017-05-09 17:11:23','user@test.com','password','Test User 1',1,2,1,0,0),(5,'2017-05-14 05:42:42','writer2@round.glass','password','Writer 2',2,2,1,0,0),(6,'2017-05-14 05:44:05','user2@test.com','password','Test User 2',1,2,1,0,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_message`
--

DROP TABLE IF EXISTS `user_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `message` mediumtext,
  `user_id` int(10) unsigned DEFAULT NULL,
  `paper_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_message_user_idx` (`user_id`),
  KEY `FK_user_message_paper_idx` (`paper_id`),
  CONSTRAINT `FK_user_message_paper` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`id`),
  CONSTRAINT `FK_user_message_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_message`
--

LOCK TABLES `user_message` WRITE;
/*!40000 ALTER TABLE `user_message` DISABLE KEYS */;
INSERT INTO `user_message` VALUES (1,'2017-05-14 05:59:54','Hi Test User',3,1),(2,'2017-05-14 06:00:06','I have written the first version of the test.',3,1),(3,'2017-05-14 06:00:10','Please have a look',3,1),(4,'2017-05-14 06:00:23','and let me know if any updates are required',3,1),(5,'2017-05-14 06:00:25','Thanks',3,1),(6,'2017-05-14 06:03:11','Hi Test User',3,4),(7,'2017-05-14 06:03:15','I have written the first version of the test.',3,4),(8,'2017-05-14 06:03:20','Please have a look',3,4),(9,'2017-05-14 06:03:23','and let me know if any updates are required',3,4),(10,'2017-05-14 06:03:27','Thanks',3,4),(11,'2017-05-14 06:04:06','Hi Test User',3,3),(12,'2017-05-14 06:04:10','I have written the first version of the test.',3,3),(13,'2017-05-14 06:04:13','Please have a look',3,3),(14,'2017-05-14 06:04:18','and let me know if any updates are required',3,3),(15,'2017-05-14 06:04:23','Thanks',3,3),(16,'2017-05-14 06:09:43','Sure',4,3),(17,'2017-05-14 06:10:00','Paper looks good.',4,3),(18,'2017-05-14 06:10:18','Can u just add one paragraph',4,3),(19,'2017-05-14 06:10:25','about when the text started',4,3),(20,'2017-05-14 06:11:07','Done',3,3),(21,'2017-05-14 06:11:11','please check',3,3),(22,'2017-05-14 06:11:43','Sure',4,1),(23,'2017-05-14 06:11:51','Paper looks good',4,1),(24,'2017-05-14 06:12:03','Can u just add one paragraph',4,1),(25,'2017-05-14 06:12:12','about when the text started',4,1),(26,'2017-05-14 06:12:27','sURE',4,4),(27,'2017-05-14 06:12:33','Paper looks good',4,4),(28,'2017-05-14 06:12:45','Can u just add one paragraph',4,4),(29,'2017-05-14 06:12:56','about when the text started',4,4),(30,'2017-05-14 06:13:10','Done',3,1),(31,'2017-05-14 06:13:14','Please check',3,1),(32,'2017-05-14 06:13:25','Done',3,4),(33,'2017-05-14 06:13:27','Please check',3,4),(34,'2017-05-14 06:13:47','Seems great',4,3),(35,'2017-05-14 06:13:53','marking the paper as complete',4,3),(36,'2017-05-14 06:13:57','thanks a lot!!',4,3),(37,'2017-05-14 06:19:43','Seems great',4,4),(38,'2017-05-14 06:19:48','marking the paper as complete',4,4),(39,'2017-05-14 06:19:52','thanks a lot!!',4,4);
/*!40000 ALTER TABLE `user_message` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-14 11:51:49
