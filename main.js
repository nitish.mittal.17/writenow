var app = require('koa')();

var koaRouter = require('koa-router');
var cors = require('koa-cors');
var mount = require('koa-mount');

var render = require('co-ejs');
var path = require('path');
var Constants = require('./constants');

var config = require('./config');
var logger = require('./logger');

var redisUtils = require('./services/redisUtils');
var sessionUtils = require('./services/sessionUtils');

var http = require('http');
var https = require('https');
var fs = require('fs');

app.use(cors({
    origin: function (req) {
      return '*';
    },
    allowMethods: ['GET', 'POST']
}));

app.use(function* (next) {
    var sessionId = this.cookies.get("SESSION_ID");
    this.currentUser = yield sessionUtils.getCurrentUser(sessionId);
    var loginPageUrl = "/app/login";

    var requestUrl = this.request.url;
    if(requestUrl != loginPageUrl && requestUrl.indexOf("static") < 0) {
        if(this.currentUser == null) {
            this.redirect(loginPageUrl);
        } else {
            yield next;
        }
    } else {
        yield next;
    }
});

app.use(function* (next) {
    var locals = {
        currentUser: this.currentUser,
        title: "Write Now",
        utils: require('./ejsHelpers'),
        paperStatus: Constants.paperStatus
    };

    render(app, {
      root: path.join(__dirname, 'views'),
      layout: false,
      viewExt: 'html',
      cache: false,
      locals: locals,
      debug: false
    });
    yield next;
});

app.use(function *(next){
  try {
    this.appRevision = (new Date).getTime();
    yield next;
  } catch (err) {
    this.type = 'json';
    logger.logError(err);
    this.status = err.status || 500;
    this.body = {'error': 'Some error occured'};
    this.app.emit('error', err, this);
  }
});

app.use(mount('/app', require('./routes/appRoutes')(app)));
app.use(mount('/api', require('./routes/apiRoutes')(app)));

/*Serve through koa-static on dev.. through nginx on prod*/
if(config.env != "prod") {
    var serve = require('koa-static');
    app.use(serve('static'));
}

module.exports = app;

require('./clusterify')(app);
