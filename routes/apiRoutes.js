var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Vendor Routes
    var vendorOverviewCtrl = require('./../controllers/vendor/OverviewCtrl');
    var vendorManageUserCtrl = require('./../controllers/vendor/ManageUserCtrl');
    var vendorBillingCtrl = require('./../controllers/vendor/BillingCtrl');

    router.post('/vendor/createUser', bodyParser, vendorManageUserCtrl.createUser);
    router.post('/vendor/assignWriter', bodyParser, vendorOverviewCtrl.assignWriterToPaper)

    //Writer Routes
    var writerOverviewCtrl = require('./../controllers/writer/OverviewCtrl');

    //User Routes
    var userOverviewCtrl = require('./../controllers/user/OverviewCtrl');

    //Paper Routes
    var paperCtrl = require('./../controllers/PaperCtrl');

    router.post('/paper/create', bodyParser, paperCtrl.createPaper);
    router.get('/paper/get', paperCtrl.getPaperData);
    router.post('/paper/save', bodyParser, paperCtrl.savePaperData);
    router.post('/paper/createMessage', bodyParser, paperCtrl.createMessage);
    router.get('/paper/getMessages', paperCtrl.getMessages);
    router.post('/paper/markComplete', bodyParser, paperCtrl.markPaperAsComplete);
    router.post('/paper/payBill', bodyParser, paperCtrl.payBill);

    //Admin Routes

    return router.middleware();
}
