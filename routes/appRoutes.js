var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    
    router.get('/login', welcomeCtrl.showLoginPage);
    router.post('/login', bodyParser, welcomeCtrl.login);
    router.get('/logout', welcomeCtrl.logout);

    //Vendor Routes
    var vendorCtrl = require('./../controllers/vendor/VendorCtrl');

    router.get('/vendor/overview', vendorCtrl.showOverviewPage);
    router.get('/vendor/manageusers', vendorCtrl.showManageUsersPage);
    router.get('/vendor/billing', vendorCtrl.showBillingPage);

    //Writer Routes
    var writerCtrl = require('./../controllers/writer/WriterCtrl');

    router.get('/writer/overview', writerCtrl.showOverviewPage);
    router.get('/writer/managepapers', writerCtrl.showManagePapersPage);
    router.get('/writer/editPaper/:paperId', writerCtrl.showEditPaperPage);

    //User Routes
    var userCtrl = require('./../controllers/user/UserCtrl');

    router.get('/user/overview', userCtrl.showOverviewPage);
    router.get('/user/managepapers', userCtrl.showManagePapersPage);
    router.get('/user/reviewPaper/:paperId', userCtrl.showReviewPaperPage);
    router.get('/user/billing', userCtrl.showBillingPage);

    //Admin Routes
    return router.middleware();
}
