var wkhtmltopdf = require('wkhtmltopdf');
var pdfjs = require('pdfjs-dist');
var HtmlDocx = require('html-docx-js');
var fs = require('fs');
var thunkify = require('thunkify');
var path = require('path');

module.exports = {
    convertHtmlToPdf: thunkify(function(htmlData, outputFilename, callback) {

        outputFilename = path.join(__dirname, '../static/static/documents/', outputFilename + '.pdf');

        wkhtmltopdf(htmlData, {output: outputFilename}, function(err, res) {
            var data = new Uint8Array(fs.readFileSync(outputFilename));
            pdfjs.getDocument(data).then(function (doc) {
                callback(err, doc.numPages);
            });
        });
    }),

    convertHtmlToWord: thunkify(function(htmlData, outputFilename, callback) {

        outputFilename = path.join(__dirname, '../static/static/documents/', outputFilename + '.docx');

        var docx = HtmlDocx.asBlob(htmlData);
        fs.writeFile(outputFilename, docx, function(err, res) {
            callback(err, res);
        });
    })
};